import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Switch, Tag } from 'ant-design-vue';
import { setRoleStatus } from '/@/api/demo/system';
import { useMessage } from '/@/hooks/web/useMessage';

export const columns: BasicColumn[] = [
  // {
  //   title: 'Code',
  //   dataIndex: 'code',
  //   fixed: 'left',
  //   width: 200,
  // },
  {
    title: 'Name',
    align: 'left',
    dataIndex: 'name',
    width: 150,
  },
  {
    title: 'Status',
    // align: 'left',
    dataIndex: 'isActive',
    width: 100,
    customRender: ({ record }) => {
      const enable = record.isActive;
      const color = enable ? 'green' : 'red';
      const text = enable ? 'Active' : 'Inactive';
      return h(Tag, { color: color }, () => text);
    },
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: 'Name',
    component: 'Input',
    colProps: { span: 8 },
  },
  // {
  //   field: 'status',
  //   label: 'Active',
  //   component: 'Switch',
  //   componentProps: {
  //     options: [
  //       { label: '启用', value: '0' },
  //       { label: '停用', value: '1' },
  //     ],
  //   },
  //   colProps: { span: 8 },
  // },
];

export const formSchema: FormSchema[] = [
  {
    field: 'code',
    label: 'Code',
    required: true,
    component: 'Input',
  },
  {
    field: 'name',
    label: 'Name',
    required: true,
    component: 'Input',
  },
  // {
  //   field: 'isActive',
  //   label: 'Active',
  //   component: 'Switch',
  //   defaultValue: true,
  // },
];
