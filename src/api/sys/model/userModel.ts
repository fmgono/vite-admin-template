/**
 * @description: Login interface parameters
 */
export interface LoginParams {
  email: string;
  password: string;
}

export interface RoleInfo {
  roleName: string;
  value: string;
}

/**
 * @description: Login interface return value
 */
export interface LoginResultModel {
  user: any;
  token: string;
  // role: RoleInfo;
}

/**
 * @description: Get user information return value
 */
export interface GetUserInfoModel {
  userId: string | number;
  callName: string;
  fullName: string;
  jobTitle: string;
  email: string;
  departmentCode: string;
  positionCode: string;
  employeeId: string | number;
  avatar?: string;
  desc?: string;
}
