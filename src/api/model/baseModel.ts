export interface BasicPageParams {
  page: number;
  pageSize: number;
}

export interface BasicFetchResult<T extends any> {
  items: T[];
  total: number;
}
// export interface PaginationFetchResult<T extends any> {
//   data: T[];
//   meta: {
//     total: number;
//     perPage: number;
//     currentPage: number;
//     lastPage: number;
//     firstPage: number;
//     firstPageUrl: string;
//     lastPageUrl: string;
//     nextPageUrl: string;
//     previousPageUrl: string;
//   };
// }
