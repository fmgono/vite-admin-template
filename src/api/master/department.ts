import { defHttp } from '/@/utils/http/axios';
import { DepartmentParams, DepartmentListGetResultModel, DepartmentForm } from './model/tableModel';

enum Api {
  list = '/master/departments',
}

/**
 * @description: Get list of resource
 */
export const getList = (params?: DepartmentParams) =>
  defHttp.get<DepartmentListGetResultModel>({
    url: Api.list,
    params,
    headers: {
      ignoreCancelToken: true,
    },
  });

/**
 * @description: Create data
 */
export const createData = (params: DepartmentForm) => {
  defHttp.post({
    url: Api.list,
    params: { ...params },
  });
};

/**
 * @description: Create data
 */
export const deleteData = (code: string) => {
  defHttp.delete({
    url: `${Api.list}/${code}`,
  });
};
