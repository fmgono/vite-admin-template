import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';
/**
 * @description: Request list interface parameters
 */
export type DepartmentParams = BasicPageParams;

export interface DepartmentListItem {
  // code: string;
  name: string;
  isActive: boolean;
}

export interface DepartmentForm {
  code: string;
  name: string;
  isActive?: boolean;
}

/**
 * @description: Request list return value
 */
export type DepartmentListGetResultModel = BasicFetchResult<DepartmentListItem>;
