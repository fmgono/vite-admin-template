import type { MenuModule } from '/@/router/types';
import { t } from '/@/hooks/web/useI18n';

const menu: MenuModule = {
  orderNo: 10,
  menu: {
    name: t('routes.master.md'),
    path: '/master-data',

    children: [
      {
        path: 'department',
        name: t('routes.master.department'),
      },
    ],
  },
};
export default menu;
