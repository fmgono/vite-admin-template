import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';

const master: AppRouteModule = {
  path: '/master-data',
  name: 'MasterData',
  component: LAYOUT,
  redirect: '/master-data/department',
  meta: {
    icon: 'ion:grid-outline',
    title: t('routes.master.md'),
  },
  children: [
    {
      path: 'department',
      name: 'Department',
      component: () => import('../../../views/master/department/index.vue'),
      meta: {
        affix: true,
        title: t('routes.master.department'),
      },
    },
  ],
};

export default master;
